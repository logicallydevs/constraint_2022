import pandas as pd
from sklearn.model_selection import train_test_split
import json
from tqdm import tqdm
from sklearn.utils import resample


def all_dataset(face_data=False):
    file_path_covid = '../data/train_data/covid/annotations/train.jsonl'
    file_path_politics = '../data/train_data/uspolitics/annotations/train.jsonl'

    json_data_train = []

    with open(file_path_covid, 'r') as json_file:
        for json_str in list(json_file):
            json_data_train.append(json.loads(json_str))

    with open(file_path_politics, 'r') as json_file:
        for json_str in list(json_file):
            json_data_train.append(json.loads(json_str))

    file_path_covid = '../data/train_data/covid/annotations/val.jsonl'
    file_path_politics = '../data/train_data/uspolitics/annotations/val.jsonl'

    json_data_val = []

    with open(file_path_covid, 'r') as json_file:
        for json_str in list(json_file):
            json_data_val.append(json.loads(json_str))

    with open(file_path_politics, 'r') as json_file:
        for json_str in list(json_file):
            json_data_val.append(json.loads(json_str))

    train_face_dict, val_face_dict = load_faces_data()

    original = []
    text = []
    word = []
    role = []

    for vals in tqdm(json_data_train):
        sentence = vals['OCR'].lower().replace('\n', ' ')
        for keys in ['hero', 'villain', 'victim', 'other']:
            for word_val in vals[keys]:
                original.append(vals['OCR'])
                if face_data:
                    text.append(sentence + '\n' + ' '.join(train_face_dict[vals['image']]))
                else:
                    text.append(sentence)
                word.append(word_val)
                role.append(keys)

    df_train = pd.DataFrame()

    df_train['sentence'] = text 
    df_train['original'] = original 
    df_train['word'] = word 
    df_train['role'] = role

    original = []
    text = []
    word = []
    role = []

    for vals in tqdm(json_data_val):
        sentence = vals['OCR'].lower().replace('\n', ' ')
        for keys in ['hero', 'villain', 'victim', 'other']:
            for word_val in vals[keys]:
                original.append(vals['OCR'])
                if face_data:
                    text.append(sentence + '\n' + ' '.join(val_face_dict[vals['image']]))
                else:
                    text.append(sentence)
                word.append(word_val)
                role.append(keys)

    df_val = pd.DataFrame()

    df_val['sentence'] = text 
    df_val['original'] = original 
    df_val['word'] = word 
    df_val['role'] = role

    df_other = df_train[df_train.role=='other']

    df_hero = df_train[df_train.role=='hero']
    df_villian = df_train[df_train.role=='villain']
    df_victim = df_train[df_train.role=='victim']

    df_hero_upsampled = resample(df_hero, 
                                    replace=True,     # sample with replacement
                                    n_samples=2000,    # to match majority class
                                    random_state=42) # reproducible results

    df_other = pd.concat([df_other, df_hero])
    df_other = pd.concat([df_other, df_hero_upsampled])

    df_villian_upsampled = resample(df_villian, 
                                    replace=True,     # sample with replacement
                                    n_samples=2000,    # to match majority class
                                    random_state=42) # reproducible results

    df_other = pd.concat([df_other, df_villian])
    df_other = pd.concat([df_other, df_villian_upsampled])

    df_victim_upsampled = resample(df_victim, 
                                    replace=True,     # sample with replacement
                                    n_samples=2000,    # to match majority class
                                    random_state=42) # reproducible results

    df_train_final = pd.concat([df_other, df_victim])
    df_train_final = pd.concat([df_train_final, df_victim_upsampled])

    return df_train_final, df_val





def load_faces_data():
    train_face_dict = {}
    val_face_dict = {}

    covid_file_path  = '../data/covid_faces'
    uspol_file_path = '../data/uspol_faces'

    json_data_train = []

    with open(covid_file_path + '/train-subImages-infos.jsonl', 'r') as json_file:
        for json_str in list(json_file):
            json_data_train.append(json.loads(json_str))

    with open(uspol_file_path + '/train-subImages-infos.jsonl', 'r') as json_file:
        for json_str in list(json_file):
            json_data_train.append(json.loads(json_str))

    for vals in json_data_train:
        train_face_dict[vals['image']] = vals['faces']



    json_data_val = []

    with open(covid_file_path + '/val-subImages-infos.jsonl', 'r') as json_file:
        for json_str in list(json_file):
            json_data_val.append(json.loads(json_str))

    with open(uspol_file_path + '/val-subImages-infos.jsonl', 'r') as json_file:
        for json_str in list(json_file):
            json_data_val.append(json.loads(json_str))

    for vals in json_data_val:
        val_face_dict[vals['image']] = vals['faces']

    return train_face_dict, val_face_dict


def get_gold_label_test(faces_data=False):
    file_path = '../data/constraint22_dataset_unseen_test_gold_labels.jsonl'

    json_data_val = []

    with open(file_path, 'r') as json_file:
        for json_str in list(json_file):
            json_data_val.append(json.loads(json_str))


    test_face_dict = {}
    face_json_data = []

    with open('../data/test_faces/faces.jsonl', 'r') as json_file:
        for json_str in list(json_file):
            face_json_data.append(json.loads(json_str))

    for vals in face_json_data:
        test_face_dict[vals['image']] = vals['faces']

    original = []
    text = []
    word = []
    image = []
    role = []

    for vals in tqdm(json_data_val):
        sentence = vals['OCR'].lower().replace('\n', ' ')
        for keys in ['hero', 'villain', 'victim', 'other']:
            for word_val in vals[keys]:
                original.append(vals['OCR'])
                if faces_data:
                    text.append(sentence + '\n' + ' '.join(test_face_dict[vals['image']]))
                else:    
                    text.append(sentence)
                word.append(word_val)
                role.append(keys)
                image.append(vals['image'])
    
    df_train = pd.DataFrame()

    df_train['sentence'] = text
    df_train['original'] = original
    df_train['word'] = word
    df_train['image'] = image
    df_train['role'] = role

    return df_train