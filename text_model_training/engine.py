import torch
import torch.nn as nn
from tqdm import tqdm
import numpy as np
from sklearn import metrics
import config
from test_eval import EvalTest

def calculate_accuracy(outputs, labels, type_acr = 'batch'):
    if type_acr == 'batch':
        labels = labels.view(outputs.shape[0], -1)
        labels = labels.cpu().detach().numpy().tolist()
        outputs = torch.softmax(outputs, dim=1).cpu().detach().numpy().tolist()    
    outputs = np.argmax(outputs, axis=1)
    return metrics.f1_score(labels, outputs, average='macro')



def train_fn(data_loader, model, optimizer, device, scheduler, accumulation_steps=config.ACCUMULATION_STEPS):
    model.train()
    
    total_loss = 0
    total_accuracy = 0
    with tqdm(enumerate(data_loader), unit="batch", total=len(data_loader)) as tepoch:
        for batch_index, dataset in tepoch: 
            tepoch.set_description(f"Epoch Started")
            input_ids = dataset['input_ids']
            attention_mask = dataset['attention_mask']
            token_type_ids = dataset['token_type_ids']
            sentiment_target = dataset['labels']
            input_ids = input_ids.to(device, dtype = torch.long)
            attention_mask = attention_mask.to(device, dtype = torch.long)
            token_type_ids = token_type_ids.to(device, dtype = torch.long)
            sentiment_target = sentiment_target.to(device, dtype = torch.long)

            optimizer.zero_grad()

            outputs = model(
                input_ids, 
                attention_mask,
                token_type_ids,
                labels=sentiment_target
                )

            logits = outputs.logits
            loss = outputs.loss
            loss.backward()

            if accumulation_steps > 1:
                loss = loss/accumulation_steps

            train_accuracy = 100.0 * calculate_accuracy(logits, sentiment_target)
            tepoch.set_postfix(loss=loss.item(), accuracy=train_accuracy)
            
            if (batch_index+1) % accumulation_steps == 0 :
                optimizer.step()
                scheduler.step()
        
            total_loss += loss.item()
            total_accuracy += train_accuracy
            
    return total_loss/len(data_loader), total_accuracy/len(data_loader)

def eval_fn(data_loader, model, device):
    model.eval()
    
    final_outputs = []
    final_targets = []

    with torch.no_grad():
        for _, dataset in tqdm(enumerate(data_loader), total=len(data_loader)): 
            input_ids = dataset['input_ids']
            attention_mask = dataset['attention_mask']
            token_type_ids = dataset['token_type_ids']
            sentiment_target = dataset['labels']

            input_ids = input_ids.to(device, dtype = torch.long)
            attention_mask = attention_mask.to(device, dtype = torch.long)
            token_type_ids = token_type_ids.to(device, dtype = torch.long)
            sentiment_target = sentiment_target.to(device, dtype = torch.float)


            outputs = model(
                input_ids, 
                attention_mask,
                token_type_ids,
                labels=sentiment_target
                )

            logits = outputs.logits

            final_targets.extend(sentiment_target.cpu().detach().numpy().tolist())
            final_outputs.extend(torch.sigmoid(logits).cpu().detach().numpy().tolist())
            
    return final_outputs, final_targets
