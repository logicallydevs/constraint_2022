# constraint_2022

The repo contains the code for the constraint meme competition. 

You can run the "training_script.py" with optional argument as to which model to train and with or without face data.
CLI :- 
```bash
training_script.py -model deberta_small -face 1
```


You can also evaluate the performance of the trained models.
CLI :- 
```bash
evaluate_test_file.py -model deberta_small -face 1
```


The f1-macro score we got the text based files :- 

- deberta small with celebrity face detection - 0.48
- deberta Small without celebrity face detection - 0.47
- deberta large with celebrity face detection - 0.57
- deberta large without celebrity face detection - 0.55


# constraint22_clean : 
this folder constains the training and evaluation script of : 
- roberta-large : Unimodal_train.py and Unimodal_eval.py
- ViLT : vilt_train.py and vilt_eval.py
- Naive Multi modal : multimodal_train.py and multimoda_eval.py
- csv folder : validation and test prediction saved in csv
- Ensembling notebook to evaluate and the evaluation and test the ensembling.


# constraint22_dataset folder :
This folder contains :
- the sub images extracted from the YOLOv5 model
- the updated annotation which contains the meta data information (face name + sub image path)


# Celebrity Detector 
The face name are extracted from the detector of this repo : https://github.com/Giphy/celeb-detection-oss

# Yolo weights
The sub image detector is saved in the folder yolo_weights.
You can use yolov5 from this repo : https://github.com/ultralytics/yolov5


