
import os
import itertools
import pandas as pd
import numpy as np
from datasets import Dataset
from datasets import load_metric
from transformers import AutoTokenizer
from transformers import AutoModelForTokenClassification, TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification, ViltPreTrainedModel
import torch
from torch import nn 
from torch.nn.utils.rnn import *
from torchvision import transforms as pth_transforms

from torch.nn import functional as F
import cv2
import json
from tqdm import tqdm
import string
import re
import nltk
import copy
from nltk.corpus import stopwords
from torch.utils.data import Dataset, DataLoader
from transformers import ViltProcessor, ViltModel, ViltConfig #ViltForImagesAndTextClassification
from transformers import DataCollatorWithPadding 
from transformers.models.vilt.modeling_vilt import ViltForImagesAndTextClassificationOutput

label_list = ["hero", "villain", "victim", "other"]
label_encoding_dict = {"hero":0, "villain":1, "victim":2, "other":3}
def upsample(texts, entity, entity2, images, faces, metadata2, labels):
    df_train = pd.DataFrame()
    df_train["role"] = labels 
    df_train["text"] = texts 
    df_train["entity"] = entity 
    df_train["entity2"] = entity2 
    df_train["image"] = images 
    df_train["faces"] = faces 
    df_train["metadata"] = metadata2

    df_other = df_train[df_train.role=='other']
    df_hero = df_train[df_train.role=='hero']
    df_villian = df_train[df_train.role=='villain']
    df_victim = df_train[df_train.role=='victim']
    df_hero_upsampled = resample(df_hero,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_other = pd.concat([df_other, df_hero])
    df_other = pd.concat([df_other, df_hero_upsampled])
    df_villian_upsampled = resample(df_villian,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_other = pd.concat([df_other, df_villian])
    df_other = pd.concat([df_other, df_villian_upsampled])
    df_victim_upsampled = resample(df_victim,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_train_final = pd.concat([df_other, df_victim])
    df_train_final = pd.concat([df_train_final, df_victim_upsampled])
    # texts, entity, entity2, images, faces, metadata2, labels
    return df_train_final.text.values.tolist(), df_train_final.entity.values.tolist(), df_train_final.entity2.values.tolist(), df_train_final.image.values.tolist(), df_train_final.faces.values.tolist(), df_train_final.metadata.values.tolist(), df_train_final.role.values.tolist()


def convert_labels2onehot(targets, label_encoding):
    N =  len(label_encoding) 
    y = np.zeros(N)
    for targ in targets:
        y[label_encoding[targ]] = 1
    return y 


def check_labels(filename):
    raw = [json.loads(x) for x in open(filename, "r")]
    matrix = np.zeros((4,4))
    for r in raw:
        dicts = {"hero":set(), "villain":set(), "victim":set(), "other":set()}
        names = ["hero", "villain", "victim", "other"]
        for i, col in enumerate(names):
            for label in r[col]:
                if i > 0:
                    for prev in range(0, i-1):
                        if label in dicts[names[prev]]:
                            matrix[i, prev] += 1
                            matrix[prev,i] += 1
                dicts[col].add(label)


    return matrix/2

def load_jsonl(filename, sub_images=False, root_dir =  ""):
    def check_image_size(path):
        img = cv2.imread(path)
        if img.shape[0] < 50 or img.shape[1] < 50:
            return False
        return True

    def order_images(paths):
        sizes = []
        images = []
        for p in paths:
            img = cv2.imread(p)
            if img.shape[0] < 50 or img.shape[1] < 50:
                continue
            else:
                images.append(p)
                sizes.append(img.shape[0] * img.shape[1])
        
        if len(images) > 0:
            ids = np.argsort(sizes)

            return [images[i] for i in reversed(ids)]
        return []

    def match_entity(sentence, entity):
        """
            make the entity match with sentence word if there is punctuation
        """

        

        def remove_punct(s1):
            exclude = set("!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ €") #set('!"#$%&\'()*+-/:;<=>?@[\\]^_`{|}~.,')
            s2 = ''
            for ch in s1:
                if ch not in exclude:
                    s2 += ch 
                else:
                    s2 += " "
            return s2.strip()
        sentence = sentence.replace("\n", " ").strip()
        #print(sentence) 
        for w in sentence.split():
            
            if w.lower()!= entity:
                #print(remove_punct(w.lower()))

                if remove_punct(w.lower()) == remove_punct(entity.lower()):
                    return w
            else:
                return w  
        
        #

        # check group of word
        entity_clean = remove_punct(entity.lower()).split()
        s = [remove_punct(x) for x in sentence.lower().split()] #remove_punct(sentence.lower())

        N = len(entity_clean)
        for i in range(len(s)-N):
            if s[i:i+N] == entity_clean:
                return  " ".join(sentence.split()[i:i+N]) 

        N = len(entity_clean)
        delta = (1, N+10)
        for i in range(len(s)-delta[0]):
            for step in range(delta[0], delta[1]+1):
                if " ".join(s[i:i+step]) == " ".join(entity_clean):
                    return  " ".join(sentence.split()[i:i+step]) 
        
        for i in range(len(s)-delta[0]):
            for step in range(delta[0], delta[1]+1):
                sentence_clean = " ".join(s[i:i+step]).split()
                for step2 in range(1, len(sentence_clean)):
                    if  " ".join(sentence_clean[:step2]) == " ".join(entity_clean):
                        return  " ".join(sentence.split()[i:i+step]) 
        # if entity_clean in s:
        #     return entity_clean, True
        return entity 

    raw = [json.loads(x) for x in open(filename, "r")]
    
    sentences = []
    entities = []
    entities_orig = []
    labels = []    
    images = []
    faces = []
    imagenet = []
    for r in raw:
        s = r["OCR"]
        for col in  ["hero", "villain", "victim", "other"]:
            for entity in r[col]:
                sentences.append(s)
                entities_orig.append(entity) 
                entities.append(match_entity(s,entity))  # 
                labels.append(col)
                #faces.append(r["faces"])
                faces.append([" ".join(face_temp.split("_")[:-1]) for face_temp in  r["faces"]])
                imagenet.append(r["imagenet22k"])
                if sub_images:
                    sub = [os.path.join( root_dir, "sub_images", x) for x in  r["sub_images"]]
                    sub = order_images(sub) #[x for x in sub if check_image_size(x) ]
                    # order sub
                    images.append(sub + [os.path.join( root_dir, "images", r["image"])]  )
                else:
                    images.append([os.path.join( root_dir, "images", r["image"])])
    return sentences, entities, entities_orig, labels, images, faces, imagenet
    
class NERDatasetMultiModal(Dataset):
    def __init__(self, sentences, entities, entities_2, labels, images, metadata, metadata2, processor, max_len=None, max_num_images=None, use_face=False, use_metadata=False,
                    preprocessing_matching="original", resize=384 ):
        self.len = len(sentences)
        self.use_face=use_face
        self.use_metadata = use_metadata
        self.sentences = sentences
        self.entities = entities
        self.entities2 = entities_2
        self.labels = labels 
        self.images = images
        self.metadata = metadata
        self.metadata2 = metadata2
        self.processor = processor
        self.max_len = max_len
        self.max_num_images = max_num_images
        self.resize = resize
        self.preprocessing_matching = preprocessing_matching
        self.transformation = pth_transforms.Compose([ 
                                                 pth_transforms.ToPILImage(),
                                                 pth_transforms.Resize((self.resize, self.resize)), 
                                                 pth_transforms.ToTensor(), 
                                                 pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)), 
                                             ]) 
    def load_images(self, paths):
        images = []
        for p in paths:
            #print(p)
            img = cv2.imread(p)
            if img.shape[0] <=50 or img.shape[1] <= 50:
                pass 
            else:
                img = self.transformation(img)
                images.append(img)
        return images
    def __getitem__(self, index):
        # step 1: get the sentence and word labels 
        sentence = self.sentences[index].strip()  
        word_labels = self.labels[index]
        if self.preprocessing_matching == "original":
            entity = self.entities[index]
        elif self.preprocessing_matching == "matching":
            entity = self.entities2[index]
        elif self.preprocessing_matching == "both": # both kind of data augmentation
            if np.random.uniform() > 0.5 : # entities lowercase
                entity = self.entities[index] 
                # if np.random.uniform() > 0.5:
                #     sentence = sentence.lower()
            else: #match entities with text
                entity = self.entities2[index]
        else:
            raise("ERROR preprocessing matching")
        faces = self.metadata[index]
        imagenet_meta = self.metadata2[index]
        img_list = self.images[index] if self.max_num_images == None or self.max_num_images >= len(self.images[index]) else  [self.images[index][-1]] if self.max_num_images == 1 else self.images[index][:self.max_num_images-1] + [self.images[index][-1]]
        images =  torch.stack(self.load_images(img_list ))
        
        
        text = sentence + " [SEP] " +  entity 
        
        
        if self.use_face:
            if len(faces)> 0:
                text = text + " [SEP] " +  " - ".join(faces) 
            else:
                text = text + " [SEP] " 
        if self.use_metadata:
            if len(imagenet_meta) > 0:
                text = text + " [SEP] " +  " ".join(imagenet_meta) 
            else:
                text = text + " [SEP] "   
        #text = text + " [SEP] "
        # step 2: use tokenizer to encode sentence (includes padding/truncation up to max length)
        #print(text)
        encoding = self.processor(text, max_length=None, return_tensors="pt") # self.max_len
        # truncate at the start of the sentence to not remove the entity information.
        if self.max_len is not None:
            remove = self.max_len - encoding["input_ids"].shape[-1]
            if remove < 0:
                remove = remove * -1 
                #print(( encoding["input_ids"][:,0]  .shape,  encoding["input_ids"][:, 1+remove:].shape))
                encoding["input_ids"] =   torch.cat((torch.as_tensor([encoding["input_ids"][:, 0]]).unsqueeze(1) , encoding["input_ids"][:, 1+remove:]), axis=-1)
                encoding["token_type_ids"] =   torch.zeros((1, len(encoding["input_ids"]))) #torch.cat((torch.as_tensor([encoding["token_type_ids"][:, 0]]).unsqueeze(1) , encoding["token_type_ids"][:, 1+remove:]), axis=-1)
                encoding["attention_mask"] =   torch.cat((torch.as_tensor([encoding["attention_mask"][:, 0]]).unsqueeze(1) , encoding["attention_mask"][:, 1+remove:]), axis=-1)
        token_id = self.processor.sep_token_id

        curr_ids = 0
        encoding["token_type_ids"] =   torch.zeros((1, encoding["input_ids"].shape[1])).long()
        for i, v in enumerate(encoding.input_ids.flatten() == token_id): # update token type ids
            encoding.token_type_ids[0, i] = curr_ids
            #print(v)
            if v:
                curr_ids += 1
        encoding["images"] = images # torch.as_tensor(images)
        encoding["labels"] =  torch.as_tensor(label_encoding_dict[word_labels])
        encoding["attention_image"] = torch.as_tensor(len(images))
        return encoding
         # encoding["input_ids"], encoding["token_type_ids"], encoding["attention_mask"], encoding["pixel_values"], encoding["pixel_mask"]

    def __len__(self):
        return len(self.sentences)

class NERDatasetVILT(Dataset):
    def __init__(self, sentences, entities, entities_2, labels, images, metadata, metadata2, processor, max_len=None, max_num_images=None, use_face=False, use_metadata=False,
                    preprocessing_matching="original", resize=384 ):
        self.len = len(sentences)
        self.use_face=use_face
        self.use_metadata = use_metadata
        self.sentences = sentences
        self.entities = entities
        self.entities2 = entities_2
        self.labels = labels 
        self.images = images
        self.metadata = metadata
        self.metadata2 = metadata2
        self.processor = processor
        self.max_len = max_len
        self.max_num_images = max_num_images
        self.resize = resize
        self.preprocessing_matching = preprocessing_matching
        self.transformation = pth_transforms.Compose([ 
                                                 pth_transforms.ToPILImage(),
                                                 pth_transforms.Resize((self.resize, self.resize)), 
                                                 pth_transforms.ToTensor(), 
                                                 pth_transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)), 
                                             ]) 
    def load_images(self, paths):
        images = []
        for p in paths:
            #print(p)
            img = cv2.imread(p)
            if img.shape[0] <=50 or img.shape[1] <= 50:
                pass 
            else:
                img = self.transformation(img)
                images.append(img)
        return images
    def __getitem__(self, index):
        # step 1: get the sentence and word labels 
        sentence = self.sentences[index].strip()  
        word_labels = self.labels[index]
        if self.preprocessing_matching == "original":
            entity = self.entities[index]
        elif self.preprocessing_matching == "matching":
            entity = self.entities2[index]
        elif self.preprocessing_matching == "both": # both kind of data augmentation
            if np.random.uniform() > 0.5 : # entities lowercase
                entity = self.entities[index] 
                # if np.random.uniform() > 0.5:
                #     sentence = sentence.lower()
            else: #match entities with text
                entity = self.entities2[index]
        else:
            raise("ERROR preprocessing matching")
        faces = self.metadata[index]
        imagenet_meta = self.metadata2[index]
        img_list = self.images[index] if self.max_num_images == None or self.max_num_images >= len(self.images[index]) else  [self.images[index][-1]] if self.max_num_images == 1 else self.images[index][:self.max_num_images-1] + [self.images[index][-1]]
        images =  self.load_images(img_list )
        
        
        text = sentence + " [SEP] " +  entity 
        
        
        if self.use_face:
            if len(faces)> 0:
                text = text + " [SEP] " +  " - ".join(faces) 
            else:
                text = text + " [SEP] " 
        if self.use_metadata:
            if len(imagenet_meta) > 0:
                text = text + " [SEP] " +  " ".join(imagenet_meta) 
            else:
                text = text + " [SEP] "   
        #text = text + " [SEP] "
        # step 2: use tokenizer to encode sentence (includes padding/truncation up to max length)
        #print(text)
        encoding = self.processor(images, text, max_length=None, return_tensors="pt")
        # truncate at the start of the sentence to not remove the entity information.
        if self.max_len is not None:
            remove = self.max_len - encoding["input_ids"].shape[-1]
            if remove < 0:
                remove = remove * -1 
                #print(( encoding["input_ids"][:,0]  .shape,  encoding["input_ids"][:, 1+remove:].shape))
                encoding["input_ids"] =   torch.cat((torch.as_tensor([encoding["input_ids"][:, 0]]).unsqueeze(1) , encoding["input_ids"][:, 1+remove:]), axis=-1)
                encoding["token_type_ids"] =   torch.zeros((1, len(encoding["input_ids"]))) #torch.cat((torch.as_tensor([encoding["token_type_ids"][:, 0]]).unsqueeze(1) , encoding["token_type_ids"][:, 1+remove:]), axis=-1)
                encoding["attention_mask"] =   torch.cat((torch.as_tensor([encoding["attention_mask"][:, 0]]).unsqueeze(1) , encoding["attention_mask"][:, 1+remove:]), axis=-1)
        token_id = self.processor.tokenizer.sep_token_id

        curr_ids = 0
        encoding["token_type_ids"] =   torch.zeros((1, encoding["input_ids"].shape[1])).long()
        for i, v in enumerate(encoding.input_ids.flatten() == token_id): # update token type ids
            encoding.token_type_ids[0, i] = curr_ids
            #print(v)
            if v:
                curr_ids += 1
        #encoding["images"] = images # torch.as_tensor(images)
        encoding["labels"] =  torch.as_tensor(label_encoding_dict[word_labels])
        #encoding["attention_image"] = torch.as_tensor(len(images))
        return encoding
         # encoding["input_ids"], encoding["token_type_ids"], encoding["attention_mask"], encoding["pixel_values"], encoding["pixel_mask"]

    def __len__(self):
        return len(self.sentences)
def processing_function_fast(batch, processor, max_length_img=20):
    # convert dict of lists to list of dicts
    #print(batch) # list of dict
    def padding(inputs, processor, max_length):

        
        inputs_pad = processor.pad_token_id
        #print(len(inputs))
        for i in range(len(inputs["input_ids"])):
            N =  max_length["input_ids"] - inputs["input_ids"][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs["input_ids"][i].device).unsqueeze(0)
                #print(padded.shape, inputs[i].shape)
                inputs["input_ids"][i] = torch.cat( (inputs["input_ids"][i], padded), dim=-1 )
        inputs["input_ids"] = torch.cat(inputs["input_ids"], dim=0)
         
            
        #print(len(inputs))
        for i in range(len(inputs['token_type_ids'])):
            inputs_pad = inputs['token_type_ids'][i][0, -1]
            N =  max_length['token_type_ids'] - inputs['token_type_ids'][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs['token_type_ids'][i].device).unsqueeze(0)
                #print(padded.shape, inputs['token_type_ids'][i].shape)
                inputs['token_type_ids'][i] = torch.cat( (inputs['token_type_ids'][i], padded), dim=-1 )
               # print(inputs['token_type_ids'][i].shape)
        inputs['token_type_ids'] = torch.cat(inputs['token_type_ids'], dim=0)
             
        #print(len(inputs))
        for i in range(len(inputs['attention_mask'])):
            inputs_pad = 0
            N =  max_length['attention_mask'] - inputs['attention_mask'][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs['attention_mask'][i].device).unsqueeze(0)
                #print(padded.shape, inputs[i].shape)
                inputs['attention_mask'][i] = torch.cat( (inputs['attention_mask'][i], padded), dim=-1 )
        inputs['attention_mask'] = torch.cat(inputs['attention_mask'], dim=0)

        for i in range(len(inputs["images"])):
        
            N =  max_length["images"] - inputs["images"][i].shape[0]
            if N > 0:
                inputs_pad = torch.zeros((N, inputs["images"][i].shape[1], inputs["images"][i].shape[2], inputs["images"][i].shape[3])).to(inputs["images"][i].device)
                #print("pixel value shape, padding/input :", inputs_pad.shape, inputs[i].shape)
                inputs["images"][i] = torch.cat( (inputs["images"][i], inputs_pad), dim=0 )
                #print(inputs[i].shape)

        inputs["images"] = torch.stack(inputs["images"], dim=0)
            
        # inputs["mask_attention"] = []
        # for i in range(len(inputs["pixel_mask"])):
        #     M = inputs["pixel_mask"][i].shape[0]
        #     N =  max_length["pixel_mask"]- inputs["pixel_mask"][i].shape[0]
        #     if N > 0:
        #         inputs_pad = torch.ones((N, inputs["pixel_mask"][i].shape[1], inputs["pixel_mask"][i].shape[2])).to(inputs["pixel_mask"][i].device)
        #         #print(padded.shape, inputs[i].shape)
        #         inputs["pixel_mask"][i] = torch.cat( (inputs["pixel_mask"][i], inputs_pad), dim=0 )
        #     #print([1]* M + [0]*N, M, N)
        #     inputs["mask_attention"].append(torch.as_tensor([1]* M + [0]*N).unsqueeze(0))

        #inputs["images"] = torch.stack(inputs["images"], dim=0)
        #print(inputs["attention_image"])
        inputs["attention_image"] = torch.stack(inputs["attention_image"], dim=0)

   
        inputs["labels"] = torch.stack(inputs["labels"], dim=0)

        return inputs
    v = dict()
    
    max_ = {} # max length seq
    for t in zip(*batch): # different
        #print("### ITERATION ### ")
        #print(t)
        t = t[0]
        v[t] = []
        max_[t] = 0
        for cpt, b in enumerate(batch):
        #    print(b["pixel_mask"].shape)
            if t  in ['input_ids', 'token_type_ids', 'attention_mask']:
                max_[t] = max(max_[t], b[t].shape[1])
            elif t == "attention_image" or t == "labels":
                pass
            else:
                max_[t] = max(max_[t], b[t].shape[0])
            v[t].append(b[t])
    
    v  = padding(inputs=v  , processor=processor, max_length=max_ )
    return v

def processing_function_fast_vilt(batch, processor, max_length_img=20):
    # convert dict of lists to list of dicts
    #print(batch) # list of dict
    def padding(inputs, processor, max_length):

        
        inputs_pad = processor.tokenizer.pad_token_id
        #print(len(inputs))
        for i in range(len(inputs["input_ids"])):
            N =  max_length["input_ids"] - inputs["input_ids"][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs["input_ids"][i].device).unsqueeze(0)
                #print(padded.shape, inputs[i].shape)
                inputs["input_ids"][i] = torch.cat( (inputs["input_ids"][i], padded), dim=-1 )
        inputs["input_ids"] = torch.cat(inputs["input_ids"], dim=0)
         
            
        #print(len(inputs))
        for i in range(len(inputs['token_type_ids'])):
            inputs_pad = inputs['token_type_ids'][i][0, -1]
            N =  max_length['token_type_ids'] - inputs['token_type_ids'][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs['token_type_ids'][i].device).unsqueeze(0)
                #print(padded.shape, inputs['token_type_ids'][i].shape)
                inputs['token_type_ids'][i] = torch.cat( (inputs['token_type_ids'][i], padded), dim=-1 )
               # print(inputs['token_type_ids'][i].shape)
        inputs['token_type_ids'] = torch.cat(inputs['token_type_ids'], dim=0)
             
        #print(len(inputs))
        for i in range(len(inputs['attention_mask'])):
            inputs_pad = 0
            N =  max_length['attention_mask'] - inputs['attention_mask'][i].shape[-1]
            if N > 0:
                padded = torch.as_tensor([inputs_pad] * N).to(inputs['attention_mask'][i].device).unsqueeze(0)
                #print(padded.shape, inputs[i].shape)
                inputs['attention_mask'][i] = torch.cat( (inputs['attention_mask'][i], padded), dim=-1 )
        inputs['attention_mask'] = torch.cat(inputs['attention_mask'], dim=0)

        for i in range(len(inputs["pixel_values"])):
        
            N =  max_length["pixel_values"] - inputs["pixel_values"][i].shape[0]
            if N > 0:
                inputs_pad = torch.zeros((N, inputs["pixel_values"][i].shape[1], inputs["pixel_values"][i].shape[2], inputs["pixel_values"][i].shape[3])).to(inputs["pixel_values"][i].device)
                #print("pixel value shape, padding/input :", inputs_pad.shape, inputs[i].shape)
                inputs["pixel_values"][i] = torch.cat( (inputs["pixel_values"][i], inputs_pad), dim=0 )
                #print(inputs[i].shape)

        inputs["pixel_values"] = torch.stack(inputs["pixel_values"], dim=0)
            
        inputs["mask_attention"] = []
        for i in range(len(inputs["pixel_mask"])):
            M = inputs["pixel_mask"][i].shape[0]
            N =  max_length["pixel_mask"]- inputs["pixel_mask"][i].shape[0]
            if N > 0:
                inputs_pad = torch.ones((N, inputs["pixel_mask"][i].shape[1], inputs["pixel_mask"][i].shape[2])).to(inputs["pixel_mask"][i].device)
                #print(padded.shape, inputs[i].shape)
                inputs["pixel_mask"][i] = torch.cat( (inputs["pixel_mask"][i], inputs_pad), dim=0 )
            #print([1]* M + [0]*N, M, N)
            inputs["mask_attention"].append(torch.as_tensor([1]* M + [0]*N).unsqueeze(0))

        inputs["pixel_mask"] = torch.stack(inputs["pixel_mask"], dim=0)
        inputs["mask_attention"] = torch.concat(inputs["mask_attention"], axis=0)

        inputs["labels"] = torch.stack(inputs["labels"], dim=0)

        return inputs
    v = dict()
    
    max_ = {} # max length seq
    for t in zip(*batch): # different
        #print("### ITERATION ### ")
        #print(t)
        t = t[0]
        v[t] = []
        max_[t] = 0
        for cpt, b in enumerate(batch):
        #    print(b["pixel_mask"].shape)
            if t  in ['input_ids', 'token_type_ids', 'attention_mask']:
                max_[t] = max(max_[t], b[t].shape[1])
            elif t == "labels":
                pass
            else:
                #print(t)
                max_[t] = max(max_[t], b[t].shape[0])
            v[t].append(b[t])

    v  = padding(inputs=v  , processor=processor, max_length=max_ )
    return v