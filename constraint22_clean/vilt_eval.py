
import os
import itertools
import pandas as pd
import numpy as np
from datasets import Dataset
from datasets import load_metric
from transformers import AutoTokenizer
from transformers import AutoModelForTokenClassification, TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification, ViltPreTrainedModel
import torch
from torch import nn 
from torch.nn.utils.rnn import *

from torch.nn import functional as F
import cv2
import json
from tqdm import tqdm
import string
import re
import nltk
import copy
from nltk.corpus import stopwords
from torch.utils.data import Dataset, DataLoader
from transformers import ViltProcessor, ViltModel, ViltConfig #ViltForImagesAndTextClassification
from transformers import DataCollatorWithPadding 
from transformers.models.vilt.modeling_vilt import ViltForImagesAndTextClassificationOutput

from model_architecture import ViltForImagesAndTextClassification
from dataset import load_jsonl,  processing_function_fast_vilt,  NERDatasetMultiModal, NERDatasetVILT
from utils import train_fn_vilt, evals_fn_vilt
import random
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

label_list = ["hero", "villain", "victim", "other"]
label_encoding_dict = {"hero":0, "villain":1, "victim":2, "other":3}

SEED = 42

def seed_everything(seed_value):
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    random.seed(seed_value)
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    
    
    if torch.cuda.is_available(): 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
seed_everything(SEED)

if __name__ == "__main__":
    from sklearn.metrics import f1_score
    class Config(object):
        def __init__(self):
            self.epochs = 100
            self.lr = 2e-5 #2e-5
            self.wd = 1e-5
            self.accumulation = 1
            self.num_images = 5
            self.batch_size= 8
            self.use_apex = True
            self.use_apex_val = False
            self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
            self.model_name = f'Vilt-image-{self.num_images}-v3'
            self.num_classes = 4
            self.verbose = True
            self.verbose_val = True
            self.smoothing = 0.0
            self.with_faces = True #True
            self.with_meta = False
            self.resample = False #True
            # optimizer 
            self.preprocessing_matching = "both" #"matching" # "mathching  both"
            self.max_seq = 275#  text max seq
            self.patience = 5
            self.factor = 0.5
            self.save_name = self.model_name + f"-adam-{self.max_seq}.pth" if self.smoothing == 0. else self.model_name + f"-adam-{self.max_seq}-smoothing.pth" 
            # match- = entities_orign -smoothing

    CFG=Config()
    if CFG.with_faces:
        CFG.save_name = CFG.save_name.replace(".pth", "-faces.pth")
    if CFG.with_meta:
        CFG.save_name = CFG.save_name.replace(".pth", "-imagenet_meta.pth")
    if CFG.resample:
        CFG.save_name = CFG.save_name.replace(".pth", "-upsample.pth")
    CFG.save_name = CFG.save_name.replace(".pth", f"-entity_{CFG.preprocessing_matching}.pth")

    import time
    print("Loading data ...")
    # TRAIN
    device = CFG.device
    texts_us, entities_us, entities_orig_us, labels_us, images_us, faces_us, imagenet_meta_us = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid, entities_covid, entities_orig_covid, labels_covid, images_covid, faces_covid, imagenet_meta_covid = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    
    texts = texts_us + texts_covid
    entities = entities_us + entities_covid
    entities_orig = entities_orig_us + entities_orig_covid 
    labels = labels_us + labels_covid 
    images = images_us + images_covid 
    faces = faces_us + faces_covid
    imagenet_meta =  imagenet_meta_us + imagenet_meta_covid

    print("Number of images max : ", max([len(im) for im in images]))
    # VAL 
    texts_us_val, entities_us_val, entities_orig_us_val, labels_us_val, images_us_val, faces_us_val, imagenet_meta_us_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid_val, entities_covid_val, entities_orig_covid_val, labels_covid_val, images_covid_val, faces_covid_val, imagenet_meta_covid_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    texts_val = texts_us_val + texts_covid_val
    entities_val = entities_us_val + entities_covid_val
    entities_orig_val = entities_orig_us_val + entities_orig_covid_val
    labels_val = labels_us_val + labels_covid_val
    images_val = images_us_val + images_covid_val
    faces_val = faces_us_val + faces_covid_val
    imagenet_meta_val =  imagenet_meta_us_val + imagenet_meta_covid_val


    # test
    texts_test, entities_test, entities_orig_test, labels_test, images_test, faces_test, imagenet_meta_test = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_test/annotations/unseen_test-faces-0.95-subImages-imagenet22k_infos-golds.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_test")
    


    # join and convert labels
    texts = [" ".join(text.split("\n"))  for text in texts]
    texts_val = [" ".join(text.split("\n"))  for text in texts_val]
    texts_test = [" ".join(text.split("\n"))  for text in texts_test]
    print("Config files")
    print(len(texts))
    cfg = ViltConfig.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")
    print(cfg)
    cfg.num_labels = CFG.num_classes
    cfg.type_vocab_size = 5 # Number of SEP NEED 3 min
    cfg.max_position_embeddings = CFG.max_seq# seq text
    cfg.num_images=CFG.num_images
    cfg.modality_type_vocab_size= cfg.modality_type_vocab_size + cfg.num_images
    cfg.merge_with_attentions = True
    processor = ViltProcessor.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")
    
    loss_fn = torch.nn.CrossEntropyLoss(label_smoothing=CFG.smoothing) 
    model =  ViltForImagesAndTextClassification(cfg) #ViltModel(cfg)
    model.load_state_dict(torch.load(CFG.save_name))

    model = model.to(device)


    dataset = NERDatasetVILT(sentences=texts, entities=entities_orig, entities_2=entities, labels=labels, images=images, metadata=faces, metadata2=imagenet_meta , processor=processor,   
                                max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="original")
    dataloader = DataLoader(dataset, num_workers=8, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))

    dataset_val = NERDatasetVILT(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="original")
    dataloader_val = DataLoader(dataset_val, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))
    dataset_val2 = NERDatasetVILT(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="matching")
    dataloader_val2 = DataLoader(dataset_val2, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))

    dataset_test = NERDatasetVILT(sentences=texts_test, entities=entities_orig_test, entities_2=entities_test, labels=labels_test, images=images_test, metadata=faces_test , metadata2=imagenet_meta_test, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="original")

    dataloader_test = DataLoader(dataset_test, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))
   



     # dataframe tr

    df_tr =  pd.DataFrame()
    df_tr["text"] = texts
    df_tr["entity"] = entities_orig
    df_tr["entity_matching"] = entities
    df_tr["label"] = labels 
    df_tr["images"] = images
    df_tr["image"] = [os.path.basename(im[-1]) for im in images]
    df_tr["faces"] = faces
    df_tr["imagenet_meta"] = imagenet_meta
    # dataframe val
    df_val =  pd.DataFrame()
    df_val["text"] = texts_val
    df_val["entity"] = entities_orig_val
    df_val["entity_matching"] = entities_val
    df_val["label"] = labels_val 
    df_val["images"] = images_val 
    df_val["image"] = [os.path.basename(im[-1]) for im in images_val]
    df_val["faces"] = faces_val
    df_val["imagenet_meta"] = imagenet_meta_val
    # dataframe test

    df_test =  pd.DataFrame()
    df_test["text"] = texts_test
    df_test["entity"] = entities_orig_test
    df_test["entity_matching"] = entities_test
    df_test["label"] = labels_test
    df_test["images"] = images_test  
    df_test["image"] = [os.path.basename(im[-1]) for im in images_test]
    df_test["faces"] = faces_test
    df_test["imagenet_meta"] = imagenet_meta_test


    label_list2 = [x + "_matching" for x in label_list]
    best_score = 0
    best_score2 = 0
    
    # VALIDATION
    print("---- Validation Score ---- ")
    preds, y_val = evals_fn_vilt(model, dataloader_val, CFG, loss_fn, verbose=True)
    preds_class = preds.argmax(1)
    scores = f1_score(y_val, preds_class, average='macro')
    print("Original :", scores )
    df_val["prediction"] = [label_list[x] for x in preds_class]
    for ck in range(len(label_list)):
        df_val[label_list[ck]] = preds[:, ck]
    df_val.to_csv(CFG.save_name.replace(".pth", "_original_results-val.csv") , index=False)

    # matching entity
    # preds, y_val = evals_fn(model, dataloader_val2, CFG, loss_fn, verbose=True)
    # preds_class = preds.argmax(1)
    # scores = f1_score(y_val, preds_class, average='macro')
    # print("matching  : ", scores)
    # df_val["prediction"] = [label_list[x] for x in preds_class]
    # for ck in range(len(label_list)):
    #     df_val[label_list[ck]] = preds[:, ck]
    # df_val.to_csv(CFG.save_name.replace(".pth", "_matching_results-val.csv") , index=False)

    # Training
    print("---- Training Score ---- ")

    # # matching entity
    # preds, y_tr = evals_fn_vilt(model, dataloader2, CFG, loss_fn, verbose=True)
    # preds_class = preds.argmax(1)
    # scores = f1_score(y_tr, preds_class, average='macro')
    # print("matching  : ", scores)
    # df_tr["prediction"] = [label_list[x] for x in preds_class]
    # for ck in range(len(label_list)):
    #     df_tr[label_list[ck]] = preds[:, ck]
    # df_tr.to_csv(CFG.save_name.replace(".pth", "_matching_results-train.csv") , index=False)


    
    # preds, y_tr = evals_fn_vilt(model, dataloader, CFG, loss_fn, verbose=True)
    # preds_class = preds.argmax(1)
    # scores = f1_score(y_tr, preds_class, average='macro')
    # print("Original :", scores )
    # df_tr["prediction"] = [label_list[x] for x in preds_class]
    # for ck in range(len(label_list)):
    #     df_tr[label_list[ck]] = preds[:, ck]
    # df_tr.to_csv(CFG.save_name.replace(".pth", "_original_results-train.csv") , index=False)

    


    # Test

    print("---- Inference on Test ---- ")
    preds, y_test = evals_fn_vilt(model, dataloader_test, CFG, loss_fn, verbose=True)
    preds_class = preds.argmax(1)
    scores = f1_score(y_test, preds_class, average='macro')
    print("Original :", scores )
    df_test["prediction"] = [label_list[x] for x in preds_class]
    for ck in range(len(label_list)):
        df_test[label_list[ck]] = preds[:, ck]
    df_test.to_csv(CFG.save_name.replace(".pth", "_original_results-test.csv") , index=False)

    # # matching entity
    # preds, _ = evals_fn(model, dataloader_test2, CFG, loss_fn, verbose=True)
    # preds_class = preds.argmax(1)

    # df_test["prediction"] = [label_list[x] for x in preds_class]
    # for ck in range(len(label_list)):
    #     df_test[label_list[ck]] = preds[:, ck]
    # df_test.to_csv(CFG.save_name.replace(".pth", "_matching_results-test.csv") , index=False)


