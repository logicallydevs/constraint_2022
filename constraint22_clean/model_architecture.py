
import os
import itertools
import pandas as pd
import numpy as np
from datasets import Dataset
from datasets import load_metric
from transformers import AutoTokenizer
from transformers import AutoModelForTokenClassification, TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification, ViltPreTrainedModel
import torch
from torch import nn 
from torch.nn.utils.rnn import *

from torch.nn import functional as F
import cv2
import json
from tqdm import tqdm
import string
import re
import nltk
import copy
from nltk.corpus import stopwords
from torch.utils.data import Dataset, DataLoader
from transformers import ViltProcessor, ViltModel, ViltConfig #ViltForImagesAndTextClassification
from transformers import DataCollatorWithPadding 
from transformers.models.vilt.modeling_vilt import ViltForImagesAndTextClassificationOutput


class AttentionBlock(nn.Module):
    def __init__(self, hidden_size, padding_value=0):
        super(AttentionBlock, self).__init__()
        
        self.W = nn.Linear(hidden_size, hidden_size)
        self.u_s = nn.Linear(hidden_size, 1)
        self.tanh = nn.Tanh()
        self.padding_value = padding_value
    def forward(self, h_i_pack):
        """
            input is a pack seq format
        """
        
        h_i, _ = pad_packed_sequence(h_i_pack, batch_first=True, padding_value=self.padding_value)
        
        u_i = self.W(h_i_pack[0])
        u_i = self.tanh(u_i)
        
        u_i = self.u_s(u_i)# torch.matmul(u_i, self.u_s)
        u_i = PackedSequence(u_i, h_i_pack[1])
        u_i, _ = pad_packed_sequence(u_i, batch_first=True, padding_value=-float("inf"))

        a_i =F.softmax(u_i, dim=1)#  F.sigmoid(u_i)# 

        v = (a_i * h_i).sum(1)        
        
        return v, a_i

class GlobalAttention(nn.Module):
    def __init__(self, hidden_size):
        super(GlobalAttention , self).__init__()
        
        self.W = nn.Linear(hidden_size, hidden_size, bias=False)
    def forward(self, h_i):
        u_i = self.W(h_i)
        a_i =  F.softmax(u_i, dim=1)#µ/2.0

        v = (a_i * h_i).sum(1)        
        return v, a_i
class ViltForImagesAndTextClassification(ViltPreTrainedModel):
    def __init__(self, config):
        super().__init__(config)

        self.num_labels = config.num_labels
        self.vilt = ViltModel(config)

        # Classifier head
        num_images = config.num_images
        self.merge_with_attentions = config.merge_with_attentions

        if config.merge_with_attentions and num_images > 1:
            self.merge_attention =   AttentionBlock(config.hidden_size) #AttBlock(config.hidden_size * num_images, config.hidden_size, activation='sigmoid', temperature=1.)
            self.classifier = nn.Sequential(
                nn.Linear(config.hidden_size, config.hidden_size ),
                nn.LayerNorm(config.hidden_size ),
                nn.GELU(),
                nn.Linear(config.hidden_size, config.num_labels),
            )
        else:
            self.classifier = nn.Sequential(
                nn.Linear(config.hidden_size * num_images, config.hidden_size * num_images),
                nn.LayerNorm(config.hidden_size * num_images),
                nn.GELU(),
                nn.Linear(config.hidden_size * num_images, config.num_labels),
            )
            # Initialize weights and apply final processing
        self.post_init()

    #@add_start_docstrings_to_model_forward(VILT_INPUTS_DOCSTRING)
    #@replace_return_docstrings(output_type=ViltForImagesAndTextClassificationOutput, config_class=_CONFIG_FOR_DOC)
    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        token_type_ids=None,
        pixel_values=None,
        pixel_mask=None,
        head_mask=None,
        inputs_embeds=None,
        image_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
        mask_attention=None,
        return_dict=None,
    ):
        r"""
        labels (`torch.LongTensor` of shape `(batch_size,)`, *optional*):
            Binary classification labels.

        Returns:

        Examples:

        ```python
        >>> from transformers import ViltProcessor, ViltForImagesAndTextClassification
        >>> import requests
        >>> from PIL import Image

        >>> image1 = Image.open(requests.get("https://lil.nlp.cornell.edu/nlvr/exs/ex0_0.jpg", stream=True).raw)
        >>> image2 = Image.open(requests.get("https://lil.nlp.cornell.edu/nlvr/exs/ex0_1.jpg", stream=True).raw)
        >>> text = "The left image contains twice the number of dogs as the right image."

        >>> processor = ViltProcessor.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")
        >>> model = ViltForImagesAndTextClassification.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")

        >>> # prepare inputs
        >>> encoding = processor([image1, image2], text, return_tensors="pt")

        >>> # forward pass
        >>> outputs = model(input_ids=encoding.input_ids, pixel_values=encoding.pixel_values.unsqueeze(0))
        >>> logits = outputs.logits
        >>> idx = logits.argmax(-1).item()
        >>> print("Predicted answer:", model.config.id2label[idx])
        Predicted answer: True
        ```"""
        output_attentions = output_attentions if output_attentions is not None else self.config.output_attentions
        output_hidden_states = (
            output_hidden_states if output_hidden_states is not None else self.config.output_hidden_states
        )
        return_dict = return_dict if return_dict is not None else self.config.use_return_dict

        if pixel_values.ndim == 4:
            # add dummy num_images dimension
            pixel_values = pixel_values.unsqueeze(1)

        num_images = pixel_values.shape[1]
        # if num_images != self.config.num_images:
        #     raise ValueError(
        #         "Make sure to match the number of images in the model with the number of images in the input."
        #     )
        pooler_outputs = []
        hidden_states = [] if output_hidden_states else None
        attentions = [] if output_attentions else None
        for i in range(num_images):
            # forward every image through the model
            outputs = self.vilt(
                input_ids,
                attention_mask=attention_mask,
                token_type_ids=token_type_ids,
                pixel_values=pixel_values[:, i, :, :, :],
                pixel_mask=pixel_mask[:, i, :, :] if pixel_mask is not None else None,
                head_mask=head_mask,
                inputs_embeds=inputs_embeds,
                image_embeds=image_embeds,
                image_token_type_idx=i + 1,
                output_attentions=output_attentions,
                output_hidden_states=output_hidden_states,
                return_dict=return_dict,
            )
            pooler_output = outputs.pooler_output if return_dict else outputs[1]
            pooler_outputs.append(pooler_output)
            if output_hidden_states:
                hidden_states.append(outputs.hidden_states)
            if output_attentions:
                attentions.append(outputs.attentions)

        pooled_output = torch.stack(pooler_outputs, dim=1)
        if pooled_output.shape[1] == 1: # 1 image only
            pooled_output = pooled_output.squeeze(1)
        if self.merge_with_attentions and num_images > 1:
            lengths = mask_attention.sum(1)
            #ids_sorted = torch.argsort(lengths, descending=True)
            #ids_resorted = torch.argsort(ids_sorted)
            pooled_output_pack = pack_padded_sequence(pooled_output, batch_first=True, lengths=lengths, enforce_sorted=False)
            #print("before attention : ", pooled_output.shape)
            pooled_output, attn_out = self.merge_attention(pooled_output_pack)
            #pooled_output = pooled_output[ids_resorted]
            #attn_out = attn_out[ids_resorted]
            #print("after attention : ", pooled_output.shape)
            #print("attn : ", attn_out, lengths)
        logits = self.classifier(pooled_output)

        loss = None
        if labels is not None:
            loss_fct = CrossEntropyLoss()
            loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))

        if not return_dict:
            output = (logits, hidden_states, attentions)
            return ((loss,) + output) if loss is not None else output

        return ViltForImagesAndTextClassificationOutput(
            loss=loss,
            logits=logits,
            hidden_states=hidden_states,
            attentions=attentions,
        )


class MultiModal(torch.nn.Module):

    def __init__(self, backbone_nlp, backbone_img, dim1, dim2, num_classe):
        super(MultiModal, self).__init__()
        self.backbone_nlp = backbone_nlp 
        self.backbone_img = backbone_img

        self.attn = AttentionBlock(hidden_size=dim2)
        self.hd  = nn.Linear(dim1+ dim2, 1024) # 
        self.fc = nn.Linear(1024, num_classe)    
    def forward(self, inputs_ids, attention_mask, token_type_ids, images, attention_image):
        output_nlp = self.backbone_nlp(input_ids=inputs_ids, attention_mask=attention_mask, token_type_ids=token_type_ids).last_hidden_state[:,0]
        output_images = []
        for i in range(images.shape[1]):
            features = self.backbone_img(images[:,i])
            output_images.append(features)
        output_images = torch.stack(output_images, dim=1)

        if images.shape[1] == 1:
            output_images = output_images.squeeze(1)
        else:
            #print(output_images.shape)
            output_images_pack = pack_padded_sequence(output_images, batch_first=True, lengths=attention_image, enforce_sorted=False)
            #print("before attention : ", pooled_output.shape)
            output_images, attn_out = self.attn(output_images_pack)
        concat_features = torch.cat((output_nlp, output_images), dim=1)

        concat_features = F.dropout(concat_features, p=0.5, training=self.training) #F.dropout(output_nlp, p=0.5, training=self.training)#
        hidden_out = F.relu(self.hd(concat_features))
        return self.fc(hidden_out)

class UniModal(torch.nn.Module):

    def __init__(self, backbone_nlp,  dim1,  num_classe):
        super(UniModal, self).__init__()
        self.backbone_nlp = backbone_nlp 

        self.hd  = nn.Linear(dim1, 1024) 
        self.fc = nn.Linear(1024, num_classe)    
    def forward(self, inputs_ids, attention_mask, token_type_ids ):
        output_nlp = self.backbone_nlp(input_ids=inputs_ids, attention_mask=attention_mask, token_type_ids=token_type_ids).last_hidden_state[:,0]

        concat_features = F.dropout(output_nlp, p=0.5, training=self.training) 
        hidden_out = F.relu(self.hd(concat_features))
        return self.fc(hidden_out)