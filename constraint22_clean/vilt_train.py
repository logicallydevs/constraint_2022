
import os
import itertools
import pandas as pd
import numpy as np
from datasets import Dataset
from datasets import load_metric
from transformers import AutoTokenizer
from transformers import AutoModelForTokenClassification, TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification, ViltPreTrainedModel
import torch
from torch import nn 
from torch.nn.utils.rnn import *

from torch.nn import functional as F
import cv2
import json
from tqdm import tqdm
import string
import re
import nltk
import copy
from nltk.corpus import stopwords
from torch.utils.data import Dataset, DataLoader
from transformers import ViltProcessor, ViltModel, ViltConfig #ViltForImagesAndTextClassification
from transformers import DataCollatorWithPadding 
from transformers.models.vilt.modeling_vilt import ViltForImagesAndTextClassificationOutput
from sklearn.utils import resample 
from model_architecture import ViltForImagesAndTextClassification
from dataset import load_jsonl,  processing_function_fast_vilt,  NERDatasetMultiModal, NERDatasetVILT
from utils import train_fn_vilt, evals_fn_vilt
import random
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

label_list = ["hero", "villain", "victim", "other"]
label_encoding_dict = {"hero":0, "villain":1, "victim":2, "other":3}

SEED = 42

def seed_everything(seed_value):
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    random.seed(seed_value)
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    
    
    if torch.cuda.is_available(): 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
seed_everything(SEED)

def upsample(texts, entity, entity2, images, faces, metadata2, labels):
    df_train = pd.DataFrame()
    df_train["role"] = labels 
    df_train["text"] = texts 
    df_train["entity"] = entity 
    df_train["entity2"] = entity2 
    df_train["image"] = images 
    df_train["faces"] = faces 
    df_train["metadata"] = metadata2

    df_other = df_train[df_train.role=='other']
    df_hero = df_train[df_train.role=='hero']
    df_villian = df_train[df_train.role=='villain']
    df_victim = df_train[df_train.role=='victim']
    df_hero_upsampled = resample(df_hero,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_other = pd.concat([df_other, df_hero])
    df_other = pd.concat([df_other, df_hero_upsampled])
    df_villian_upsampled = resample(df_villian,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_other = pd.concat([df_other, df_villian])
    df_other = pd.concat([df_other, df_villian_upsampled])
    df_victim_upsampled = resample(df_victim,
                                    replace=True,     # sample with replacement
                                    n_samples=5000,    # to match majority class
                                    random_state=42) # reproducible results
    df_train_final = pd.concat([df_other, df_victim])
    df_train_final = pd.concat([df_train_final, df_victim_upsampled])
    # texts, entity, entity2, images, faces, metadata2, labels
    return df_train_final.text.values.tolist(), df_train_final.entity.values.tolist(), df_train_final.entity2.values.tolist(), df_train_final.image.values.tolist(), df_train_final.faces.values.tolist(), df_train_final.metadata.values.tolist(), df_train_final.role.values.tolist()
if __name__ == "__main__":
    from sklearn.metrics import f1_score
    class Config(object):
        def __init__(self):
            self.epochs = 10
            self.lr = 2e-5 #2e-5
            self.wd = 1e-5
            self.accumulation = 1
            self.num_images = 5
            self.batch_size= 4 # 4# 8 for no metadata and image = 1. Faster
            self.use_apex = True
            self.use_apex_val = False
            self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
            self.model_name = f'Vilt-image-{self.num_images}-v3'
            self.num_classes = 4
            self.verbose = True
            self.verbose_val = True
            self.smoothing = 0.0
            self.with_faces = True # False #True = 0.423     # False = 0.422 num=5
            self.with_meta = False #True
            self.resample = False #True
            # optimizer 
            self.preprocessing_matching = "both" #"matching" # "mathching  both"
            self.max_seq = 275 #  text max seq 400
            self.patience = 5
            self.factor = 0.5
            self.save_name = self.model_name + f"-adam-{self.max_seq}.pth" if self.smoothing == 0. else self.model_name + f"-adam-{self.max_seq}-smoothing.pth" 
            # match- = entities_orign -smoothing

    CFG=Config()
    if CFG.with_faces:
        CFG.save_name = CFG.save_name.replace(".pth", "-faces.pth")
    if CFG.with_meta:
        CFG.save_name = CFG.save_name.replace(".pth", "-imagenet_meta.pth")
    if CFG.resample:
        CFG.save_name = CFG.save_name.replace(".pth", "-upsample.pth")
    CFG.save_name = CFG.save_name.replace(".pth", f"-entity_{CFG.preprocessing_matching}.pth")

    import time
    print("Loading data ...")
    # TRAIN
    device = CFG.device
    texts_us, entities_us, entities_orig_us, labels_us, images_us, faces_us, imagenet_meta_us = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid, entities_covid, entities_orig_covid, labels_covid, images_covid, faces_covid, imagenet_meta_covid = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    
    texts = texts_us + texts_covid
    entities = entities_us + entities_covid
    entities_orig = entities_orig_us + entities_orig_covid 
    labels = labels_us + labels_covid 
    images = images_us + images_covid 
    faces = faces_us + faces_covid
    imagenet_meta =  imagenet_meta_us + imagenet_meta_covid

    if CFG.resample:
        texts, entities, entities_orig, images, faces, imagenet_meta, labels   =  upsample(texts=texts, entity=entities, entity2=entities_orig, images=images, faces=faces, metadata2=imagenet_meta, labels=labels)
    print("Number of images max : ", max([len(im) for im in images]))
    # VAL 
    texts_us_val, entities_us_val, entities_orig_us_val, labels_us_val, images_us_val, faces_us_val, imagenet_meta_us_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid_val, entities_covid_val, entities_orig_covid_val, labels_covid_val, images_covid_val, faces_covid_val, imagenet_meta_covid_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    texts_val = texts_us_val + texts_covid_val
    entities_val = entities_us_val + entities_covid_val
    entities_orig_val = entities_orig_us_val + entities_orig_covid_val
    labels_val = labels_us_val + labels_covid_val
    images_val = images_us_val + images_covid_val
    faces_val = faces_us_val + faces_covid_val
    imagenet_meta_val =  imagenet_meta_us_val + imagenet_meta_covid_val



    # join and convert labels
    texts = [" ".join(text.split("\n"))  for text in texts]
    texts_val = [" ".join(text.split("\n"))  for text in texts_val]
 
    print("Config files")
    print(len(texts))
    cfg = ViltConfig.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")
    print(cfg)
    cfg.num_labels = CFG.num_classes
    cfg.type_vocab_size = 5 # Number of SEP NEED 3 min
    cfg.max_position_embeddings = CFG.max_seq# seq text
    cfg.num_images=CFG.num_images
    cfg.modality_type_vocab_size= cfg.modality_type_vocab_size + cfg.num_images
    cfg.merge_with_attentions = True
    processor = ViltProcessor.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2")
    checkpoint = ViltModel.from_pretrained("dandelin/vilt-b32-finetuned-nlvr2").state_dict()

    # correct some weights because of some parameters changed
    temp = checkpoint["embeddings.text_embeddings.token_type_embeddings.weight"]
    checkpoint["embeddings.text_embeddings.token_type_embeddings.weight"]  = torch.zeros((cfg.type_vocab_size, 768))
    checkpoint["embeddings.text_embeddings.token_type_embeddings.weight"][:2, :] = temp  

    temp = checkpoint["embeddings.text_embeddings.position_ids"]
    checkpoint["embeddings.text_embeddings.position_ids"]  = torch.zeros((1, cfg.max_position_embeddings))
    checkpoint["embeddings.text_embeddings.position_ids"][:, :40] = temp  

    temp = checkpoint["embeddings.text_embeddings.position_embeddings.weight"]
    checkpoint["embeddings.text_embeddings.position_embeddings.weight"]  = torch.zeros(( cfg.max_position_embeddings, 768))
    checkpoint["embeddings.text_embeddings.position_embeddings.weight"][:40 ] = temp 

    temp = checkpoint["embeddings.token_type_embeddings.weight"]
    checkpoint["embeddings.token_type_embeddings.weight"]  = torch.zeros(( cfg.modality_type_vocab_size, 768))
    checkpoint["embeddings.token_type_embeddings.weight"][:3 ] = temp 
    #del checkpoint["embeddings.text_embeddings.token_type_embeddings.weight"]

    loss_fn = torch.nn.CrossEntropyLoss(label_smoothing=CFG.smoothing) 
    model =  ViltForImagesAndTextClassification(cfg) #ViltModel(cfg)
    model.vilt.load_state_dict(checkpoint, strict=True)

    model = model.to(device)



    #data_collator = DataCollatorWithPadding(processor.tokenizer, max_length=60)
    print(imagenet_meta[0])
    dataset = NERDatasetVILT(sentences=texts, entities=entities_orig, entities_2=entities, labels=labels, images=images, metadata=faces, metadata2=imagenet_meta , processor=processor,   
                                max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching=CFG.preprocessing_matching)
    dataloader = DataLoader(dataset, num_workers=8, batch_size=CFG.batch_size, shuffle=True, collate_fn=lambda x: processing_function_fast_vilt(x, processor))

    dataset_val = NERDatasetVILT(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="original")
    dataloader_val = DataLoader(dataset_val, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))
    dataset_val2 = NERDatasetVILT(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="matching")
    dataloader_val2 = DataLoader(dataset_val2, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast_vilt(x, processor))

    print(texts_val[:5], entities_val[:5])
    optimizer = torch.optim.Adam(model.parameters(), lr =CFG.lr, amsgrad=False, weight_decay=CFG.wd)
    reducer = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='max', factor=CFG.factor, patience=CFG.patience, verbose=True, min_lr=1e-6, threshold = 1e-3)
    

    best_score = 0
    best_score2 = 0
    reducer.step(best_score)
    for e in range(CFG.epochs):
        print("Epochs : ", e)
        train_fn_vilt(model, dataloader, optimizer, loss_fn, CFG, accumulation=CFG.accumulation, verbose=CFG.verbose)
        # original entity
        preds, y_val = evals_fn_vilt(model, dataloader_val, CFG, loss_fn, verbose=True)
        preds_class = preds.argmax(1)
        scores = f1_score(y_val, preds_class, average='macro')

        if scores > best_score:
            best_score = scores 
            torch.save(model.state_dict(), CFG.save_name )
            print("Model Improved ! F1-score : ", best_score)
            
        else:
            print("Model not improved, best : ", best_score, scores)

        # matching entity
        preds, y_val = evals_fn_vilt(model, dataloader_val2, CFG, loss_fn, verbose=True)
        preds_class = preds.argmax(1)
        scores = f1_score(y_val, preds_class, average='macro')

        if scores > best_score2:
            best_score2 = scores 
            torch.save(model.state_dict(), CFG.save_name.replace(".pth", "-2.pth") )
            print("Model on matching Entity Improved ! F1-score : ", best_score2)
        else:
            print("Model not improved, best : ", best_score2, scores)


        reducer.step(scores) 
