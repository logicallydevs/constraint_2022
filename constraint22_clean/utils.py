import torch
from torch.utils.data import DataLoader, Dataset
import pandas as pd
import numpy as np
import os
import pandas as pd
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit
from sklearn.metrics import recall_score
from torchvision.transforms import *
from PIL import Image
from torch.nn.functional import cross_entropy, softmax
from torch.optim import SGD, Adam
 
from torch.optim.lr_scheduler import ReduceLROnPlateau
# (137,236,1)
import time
from tqdm import tqdm
from torch.cuda.amp import GradScaler, autocast
# train one epoch (segments only)
def train_fn_vilt(model, dataloader, optimizer, loss_fn, cfg, accumulation=2, verbose=False, scheduler=None, optimizer2=None):
    model.train()
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose )
    scaler = GradScaler()
    optimizer.zero_grad()
    N = 0.
    device = cfg.device
    for i, batch in enumerate(t):
        
        labels = batch["labels"].to(device)
        with autocast(cfg.use_apex):
            #print(batch["input_ids"].shape, batch["attention_mask"].shape, batch["token_type_ids"].shape, batch["images"].shape, batch["attention_image"].shape)
            #print(batch["attention_image"])
            logits = model(input_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),
                        pixel_values=batch["pixel_values"].to(device), pixel_mask=batch["pixel_mask"].to(device), mask_attention=batch["mask_attention"]  ).logits
            #logits = outputs.logits
            #print(outputs, labels)
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())
            loss = loss.mean()
            N += len(labels)
            
            
                
            
        if torch.isnan(loss):
            print("loss error")
            print(torch.isnan(outputs).sum())
            loss[torch.isnan(loss)] = 0.0
            
            total_loss += loss.item()
        else:
            total_loss += loss.item() 


        if cfg.use_apex:
            loss = loss/accumulation
            scaler.scale(loss).backward()
        else:
            loss = loss/accumulation
            loss.backward()

        if (i+1)%accumulation == 0 or i-1 == len(t):
            if cfg.use_apex:
                scaler.step(optimizer)
                if optimizer2 is not None:
                    scaler.step(optimizer2)
                # Updates the scale for next iteration.
                scaler.update()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.zero_grad()
            else:                
                optimizer.step()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.step()
                    optimizer2.zero_grad()
        if scheduler is not None:
            scheduler.step()
        t.set_description("Loss : {0} ".format(total_loss/(i+1) ))
        t.refresh()       

    return total_loss/N

# evaluation     segment  
def evals_fn_vilt(model, dataloader, cfg, loss_fn, verbose=False):
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose)
     
    y_true = []
    y_preds = []
    model.eval()
    device = cfg.device
    with torch.no_grad():
        for i, batch in enumerate(t):
            
            labels = batch["labels"].to(device)
            with autocast(cfg.use_apex_val):
                logits = model(input_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),
                        pixel_values=batch["pixel_values"].to(device), pixel_mask=batch["pixel_mask"].to(device), mask_attention=batch["mask_attention"] ).logits
                #logits = outputs.logits
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())

            
            loss = loss.mean()
            total_loss += loss.item() 
            
            y_true.append(labels.detach().cpu().numpy())
            y_preds.append( torch.nn.functional.softmax(logits, dim=1).cpu().detach().numpy())
            
    print("Val loss : ", total_loss/(i+1))    
    return np.concatenate(y_preds).astype(np.float32), np.concatenate(y_true).astype(np.float32) 

#Unimodal

# Unimodal 

def train_fn_unimodal(model, dataloader, optimizer, loss_fn, cfg, accumulation=2, verbose=False, scheduler=None, optimizer2=None):
    model.train()
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose )
    scaler = GradScaler()
    optimizer.zero_grad()
    N = 0.
    device = cfg.device
    for i, batch in enumerate(t):
        
        labels = batch["labels"].to(device)
        with autocast(cfg.use_apex):
            #print(batch["input_ids"].shape, batch["attention_mask"].shape, batch["token_type_ids"].shape, batch["images"].shape, batch["attention_image"].shape)
            #print(batch["attention_image"])
            logits = model(inputs_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),)
            #logits = outputs.logits
            #print(outputs, labels)
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())
            loss = loss.mean()
            N += len(labels)
            
            
                
            
        if torch.isnan(loss):
            print("loss error")
            print(torch.isnan(outputs).sum())
            loss[torch.isnan(loss)] = 0.0
            
            total_loss += loss.item()
        else:
            total_loss += loss.item() 


        if cfg.use_apex:
            loss = loss/accumulation
            scaler.scale(loss).backward()
        else:
            loss = loss/accumulation
            loss.backward()

        if (i+1)%accumulation == 0 or i-1 == len(t):
            if cfg.use_apex:
                scaler.step(optimizer)
                if optimizer2 is not None:
                    scaler.step(optimizer2)
                # Updates the scale for next iteration.
                scaler.update()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.zero_grad()
            else:                
                optimizer.step()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.step()
                    optimizer2.zero_grad()
        if scheduler is not None:
            scheduler.step()
        t.set_description("Loss : {0} ".format(total_loss/(i+1) ))
        t.refresh()       

    return total_loss/N

# evaluation     segment  
def evals_fn_unimodal(model, dataloader, cfg, loss_fn, verbose=False):
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose)
     
    y_true = []
    y_preds = []
    model.eval()
    device = cfg.device
    with torch.no_grad():
        for i, batch in enumerate(t):
            
            labels = batch["labels"].to(device)
            with autocast(cfg.use_apex_val):
                logits = model(inputs_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),)
                #logits = outputs.logits
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())

            
            loss = loss.mean()
            total_loss += loss.item() 
            
            y_true.append(labels.detach().cpu().numpy())
            y_preds.append( torch.nn.functional.softmax(logits, dim=1).cpu().detach().numpy())
            
    print("Val loss : ", total_loss/(i+1))    
    return np.concatenate(y_preds).astype(np.float32), np.concatenate(y_true).astype(np.float32) 

def train_fn_multimodal(model, dataloader, optimizer, loss_fn, cfg, accumulation=2, verbose=False, scheduler=None, optimizer2=None):
    model.train()
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose )
    scaler = GradScaler()
    optimizer.zero_grad()
    N = 0.
    device = cfg.device
    for i, batch in enumerate(t):
        
        labels = batch["labels"].to(device)
        with autocast(cfg.use_apex):
            #print(batch["input_ids"].shape, batch["attention_mask"].shape, batch["token_type_ids"].shape, batch["images"].shape, batch["attention_image"].shape)
            #print(batch["attention_image"])
            logits = model(inputs_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),
                        images=batch["images"].to(device), attention_image=batch["attention_image"]  )
            #logits = outputs.logits
            #print(outputs, labels)
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())
            loss = loss.mean()
            N += len(labels)
            
            
                
            
        if torch.isnan(loss):
            print("loss error")
            print(torch.isnan(outputs).sum())
            loss[torch.isnan(loss)] = 0.0
            
            total_loss += loss.item()
        else:
            total_loss += loss.item() 


        if cfg.use_apex:
            loss = loss/accumulation
            scaler.scale(loss).backward()
        else:
            loss = loss/accumulation
            loss.backward()

        if (i+1)%accumulation == 0 or i-1 == len(t):
            if cfg.use_apex:
                scaler.step(optimizer)
                if optimizer2 is not None:
                    scaler.step(optimizer2)
                # Updates the scale for next iteration.
                scaler.update()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.zero_grad()
            else:                
                optimizer.step()
                optimizer.zero_grad()
                if optimizer2 is not None:
                    optimizer2.step()
                    optimizer2.zero_grad()
        if scheduler is not None:
            scheduler.step()
        t.set_description("Loss : {0} ".format(total_loss/(i+1) ))
        t.refresh()       

    return total_loss/N

# evaluation     segment  
def evals_fn_multimodal(model, dataloader, cfg, loss_fn, verbose=False):
    total_loss = 0.
    t=tqdm(dataloader, disable=not verbose)
     
    y_true = []
    y_preds = []
    model.eval()
    device = cfg.device
    with torch.no_grad():
        for i, batch in enumerate(t):
            
            labels = batch["labels"].to(device)
            with autocast(cfg.use_apex_val):
                logits = model(inputs_ids=batch["input_ids"].to(device), attention_mask=batch["attention_mask"].to(device), token_type_ids=batch["token_type_ids"].to(device),
                    images=batch["images"].to(device), attention_image=batch["attention_image"]  )
                #logits = outputs.logits
            loss = loss_fn(logits, labels.long()) if type(loss_fn) == type(torch.nn.CrossEntropyLoss()) else loss_fn(outputs, labels.float())

            
            loss = loss.mean()
            total_loss += loss.item() 
            
            y_true.append(labels.detach().cpu().numpy())
            y_preds.append( torch.nn.functional.softmax(logits, dim=1).cpu().detach().numpy())
            
    print("Val loss : ", total_loss/(i+1))    
    return np.concatenate(y_preds).astype(np.float32), np.concatenate(y_true).astype(np.float32) 

