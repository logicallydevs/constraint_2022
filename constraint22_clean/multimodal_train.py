
import os
import itertools
import pandas as pd
import numpy as np
from datasets import Dataset
from datasets import load_metric
from transformers import AutoTokenizer
from transformers import AutoModelForTokenClassification, TrainingArguments, Trainer
from transformers import DataCollatorForTokenClassification, ViltPreTrainedModel
import torch
from torch import nn 
from torch.nn.utils.rnn import *
import random
from torch.nn import functional as F
import cv2
import json
from tqdm import tqdm
import string
import re
import nltk
import copy
from nltk.corpus import stopwords
from torch.utils.data import Dataset, DataLoader
from transformers import ViltProcessor, ViltModel, ViltConfig, AutoConfig, AutoModel #ViltForImagesAndTextClassification
from transformers import DataCollatorWithPadding , AdamW, get_linear_schedule_with_warmup
from transformers.models.vilt.modeling_vilt import ViltForImagesAndTextClassificationOutput
import timm
from model_architecture import ViltForImagesAndTextClassification, UniModal, MultiModal
from dataset import load_jsonl,  processing_function_fast,  NERDatasetMultiModal, NERDatasetVILT
from utils import train_fn_multimodal, evals_fn_multimodal
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

label_list = ["hero", "villain", "victim", "other"]
label_encoding_dict = {"hero":0, "villain":1, "victim":2, "other":3}

SEED = 42

def seed_everything(seed_value):
    os.environ['PYTHONHASHSEED'] = str(seed_value)
    random.seed(seed_value)
    np.random.seed(seed_value)
    torch.manual_seed(seed_value)
    
    
    if torch.cuda.is_available(): 
        torch.cuda.manual_seed(seed_value)
        torch.cuda.manual_seed_all(seed_value)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
seed_everything(SEED)


if __name__ == "__main__":
    from sklearn.metrics import f1_score
    from dataset import upsample
    class Config(object):
        def __init__(self):
            self.epochs = 20
            self.lr = 1e-5 #5e-6#5e-6#8e-6 #2e-5
            self.wd = 1e-5
            self.accumulation = 1
            self.num_images = 5
            self.batch_size=4
            self.use_apex = True
            self.use_apex_val = False
            self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
            self.model_name_nlp = f'microsoft/deberta-v3-large'# 
            self.model_name_vision = "tf_efficientnet_b7_ns"
            self.model_name = f"{self.model_name_nlp.split('/')[-1]}-{self.model_name_vision}"
            self.num_classes = 4
            self.verbose = True
            self.verbose_val = True
            self.smoothing = 0.00
            self.with_faces = True
            self.with_meta =False #  True # 
            self.resample = False # True
            # optimizer 
            self.preprocessing_matching = "both" #"matching" # "mathching  both"
            self.max_seq = 250# text max seq #250
            self.patience = 5
            self.factor = 0.5
            self.save_name = self.model_name + f"-adam-{self.max_seq}.pth" if self.smoothing == 0. else self.model_name + f"-adam-{self.max_seq}-smoothing.pth" 
            # match- = entities_orign -smoothing

    CFG=Config()
    if CFG.with_faces:
        CFG.save_name = CFG.save_name.replace(".pth", "-faces.pth")
    if CFG.with_meta:
        CFG.save_name = CFG.save_name.replace(".pth", "-imagenet_meta.pth")
    CFG.save_name = CFG.save_name.replace(".pth", f"-entity_{CFG.preprocessing_matching}.pth")
    if CFG.resample:
        CFG.save_name = CFG.save_name.replace(".pth", "-upsample.pth")
    import time
    print("Loading data ...")
    # TRAIN
    device = CFG.device
    texts_us, entities_us, entities_orig_us, labels_us, images_us, faces_us, imagenet_meta_us = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid, entities_covid, entities_orig_covid, labels_covid, images_covid, faces_covid, imagenet_meta_covid = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/train_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    
    texts = texts_us + texts_covid
    entities = entities_us + entities_covid
    entities_orig = entities_orig_us + entities_orig_covid 
    labels = labels_us + labels_covid 
    images = images_us + images_covid 
    faces = faces_us + faces_covid
    imagenet_meta =  imagenet_meta_us + imagenet_meta_covid

    print("Number of images max : ", max([len(im) for im in images]))
    if CFG.resample:
        texts, entities, entities_orig, images, faces, imagenet_meta, labels   =  upsample(texts=texts, entity=entities, entity2=entities_orig, images=images, faces=faces, metadata2=imagenet_meta, labels=labels)
    # VAL 
    texts_us_val, entities_us_val, entities_orig_us_val, labels_us_val, images_us_val, faces_us_val, imagenet_meta_us_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_uspolitics")
    texts_covid_val, entities_covid_val, entities_orig_covid_val, labels_covid_val, images_covid_val, faces_covid_val, imagenet_meta_covid_val = load_jsonl("/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19/annotations/val_ocr_match-v2-subImages-imagenet22k_infos.jsonl", sub_images=True, root_dir="/home/Shiro/Competitions/constraint22_dataset/constraint22_covid19")
    texts_val = texts_us_val + texts_covid_val
    entities_val = entities_us_val + entities_covid_val
    entities_orig_val = entities_orig_us_val + entities_orig_covid_val
    labels_val = labels_us_val + labels_covid_val
    images_val = images_us_val + images_covid_val
    faces_val = faces_us_val + faces_covid_val
    imagenet_meta_val =  imagenet_meta_us_val + imagenet_meta_covid_val



    # join and convert labels
    texts = [" ".join(text.split("\n"))  for text in texts]
    texts_val = [" ".join(text.split("\n"))  for text in texts_val]
 
    print("Config files")
    print(len(texts))
    cfg = AutoConfig.from_pretrained(CFG.model_name_nlp)
    print(cfg)
    
    #cfg.num_labels = CFG.num_classes
    cfg.type_vocab_size = 5 # Number of SEP NEED 3 min
    #cfg.max_position_embeddings = CFG.max_seq# seq text
    #cfg.num_images=CFG.num_images
    #cfg.modality_type_vocab_size= cfg.modality_type_vocab_size + cfg.num_images
    #cfg.merge_with_attentions = True
    processor = AutoTokenizer.from_pretrained(CFG.model_name_nlp)
    checkpoint = AutoModel.from_pretrained(CFG.model_name_nlp).state_dict()
    
    if "roberta" in CFG.model_name_nlp: 
        del checkpoint["embeddings.token_type_embeddings.weight"]
    network_nlp = AutoModel.from_config(cfg)
    network_nlp.load_state_dict(checkpoint, strict=False)
    
    network_img = timm.create_model(CFG.model_name_vision, pretrained=True)
    network_img.classifier = nn.Identity()
    loss_fn = torch.nn.CrossEntropyLoss(label_smoothing=CFG.smoothing) 
    model = MultiModal(backbone_nlp=network_nlp, backbone_img=network_img, dim1=1024, dim2=2560, num_classe=CFG.num_classes) #ViltForImagesAndTextClassification(cfg) #ViltModel(cfg)
    # w2 = torch.load("deberta-v3-large-v2-adam-275-faces-entity_both.pth")
    # del w2["hd.weight"]
    # model.load_state_dict(w2, strict=False)
    model = model.to(device)
    

    dataset = NERDatasetMultiModal(sentences=texts, entities=entities_orig, entities_2=entities, labels=labels, images=images, metadata=faces, metadata2=imagenet_meta , processor=processor,   
                                max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching=CFG.preprocessing_matching)
    dataloader = DataLoader(dataset, num_workers=8, batch_size=CFG.batch_size, shuffle=True, collate_fn=lambda x: processing_function_fast(x, processor))

    dataset_val = NERDatasetMultiModal(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="original")
    dataloader_val = DataLoader(dataset_val, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast(x, processor))
    dataset_val2 = NERDatasetMultiModal(sentences=texts_val, entities=entities_orig_val, entities_2=entities_val, labels=labels_val, images=images_val, metadata=faces_val , metadata2=imagenet_meta_val, 
                processor=processor,   max_len=CFG.max_seq, max_num_images=CFG.num_images, use_face=CFG.with_faces, use_metadata=CFG.with_meta, preprocessing_matching="matching")
    dataloader_val2 = DataLoader(dataset_val2, num_workers=4, batch_size=CFG.batch_size, shuffle=False, collate_fn=lambda x: processing_function_fast(x, processor))

    print(texts_val[:5], entities_val[:5])

    param_optimizer = list(model.backbone_nlp.named_parameters())


    no_decay = ["bias", "LayerNorm.bias", "LayerNorm.weight"]

    optimizer_parameters = [

    {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 2e-5},

    {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0},

    ]



    num_training_steps = int(len(dataset)/CFG.batch_size * CFG.epochs)

    optimizer = AdamW(optimizer_parameters, lr=CFG.lr)
    scheduler = get_linear_schedule_with_warmup(optimizer,num_warmup_steps = 0, num_training_steps = num_training_steps)

    optimizer2 = torch.optim.Adam(list(model.backbone_img.parameters()) + list(model.fc.parameters()) + list(model.attn.parameters()) + list(model.hd.parameters()), 
                                    lr =1e-4, amsgrad=False, weight_decay=CFG.wd)



    #label_list2 = [x + "_matching" for x in label_list]
    best_score = 0
    best_score2 = 0
    best_epoch = 0
    #reducer.step(best_score)
    for e in range(CFG.epochs):
        print("Epochs : ", e)
        train_fn_multimodal(model, dataloader, optimizer, loss_fn, CFG, accumulation=CFG.accumulation, verbose=CFG.verbose, scheduler=scheduler, optimizer2= optimizer2)
        # original entity
        preds, y_val = evals_fn_multimodal(model, dataloader_val, CFG, loss_fn, verbose=True)
        preds_class = preds.argmax(1)
        scores = f1_score(y_val, preds_class, average='macro')

        if scores > best_score:
            best_score = scores 
            print("Model Improved ! F1-score : ", best_score)
            torch.save(model.state_dict(), CFG.save_name )
            best_epoch = e
            
        else:
            print("Model not improved, best : ", best_score, scores)

        # matching entity
        preds, y_val = evals_fn_multimodal(model, dataloader_val2, CFG, loss_fn, verbose=True)
        preds_class = preds.argmax(1)
        scores = f1_score(y_val, preds_class, average='macro')

        if scores > best_score2:
            best_score2 = scores 
            print("Model on matching Entity Improved ! F1-score : ", best_score2)
            torch.save(model.state_dict(), CFG.save_name.replace(".pth", "-2.pth") )
            
        else:
            print("Model not improved, best : ", best_score2, scores)

