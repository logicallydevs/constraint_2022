import torch
from torch import nn
import timm
import json
import cv2
from tqdm import tqdm
def extract_labels(filename):
    classes = []
    with open(filename, "r") as f:
        for line in f:
            line = line.strip()
            line = " ".join(line.split(","))
            classes.append(line)
    return classes 

device = "cuda:0" if torch.cuda.is_available() else "cpu"
filename = "label_to_words.json"
int2class = json.load(open(filename, "r"))#extract_labels( "imagenet21k_wordnet_lemmas.txt" )
print("number of classe :", len(int2class))
#print(int2class)
model = timm.create_model("convnext_xlarge_in22k" , pretrained=True)
model.to(device)
print(model)


types = "test"#"uspolitics" #  "covid19" # 
files_ ="unseen_test-faces-0.95-subImages.jsonl" # "val_ocr_match-v2-subImages.jsonl"
data = [json.loads(x) for x in open(f"constraint22_{types}/annotations/{files_}", "r")]
counter = 0
with open(f"constraint22_{types}/annotations/{files_.replace('.jsonl', '-imagenet22k_infos.jsonl')}", "w") as f:
    for i, b in tqdm(enumerate(data)):
        image_path = f'constraint22_{types}/images/{b["image"]}'
        img = cv2.resize(cv2.imread(image_path), (224, 224))
        
        with torch.no_grad():
            output = model(torch.permute(torch.as_tensor(img), (2,0,1)).unsqueeze(0).float().to(device))

            output = torch.nn.functional.softmax(output).squeeze(0)
            output_classe = torch.argsort(output, descending=True).cpu().numpy()
        
        final_classe = [int2class[str(x)] for x in output_classe[:10] if output[x]>=0.1]        
        if len(final_classe) > 0:
            print(output[output_classe[:5]])
            print(b)
            print(final_classe)
            counter +=1
        b["imagenet22k"] = final_classe
        #b["villain"] = b["vilain"]
        #del b["vilain"]
        #break
        #print(b)
        #break
        json.dump(b, f)
        f.write('\n')

print("number of classe got : ", counter, i)